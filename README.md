# Customers Reviews App

#### App Features
- Reviews section of an Amazon Products page.
- Reviews can be grouped by day, week, or month.
- Reviews can be filtered by number of stars. The default 5 star.
- Reviews can be sorted by time, Most recent reviews, or last.
- Infinite scrolling. As you scroll, more reviews will be loaded.

##### Application overview
Reviews List App consists of components and containers. Uses React, Redux as a state container, build with [Material-UI](https://github.com/mui-org/material-ui) as UI Framework.

##### Implementation Approach
For implementing Filters, Sorting and Grouping functionality I choose [Selector Pattern](https://hackernoon.com/selector-pattern-painless-redux-store-destructuring-bfc26b72b9ae) approach and [Selector](https://github.com/reduxjs/reselect) library for Redux

##### Why Selector?
The main reason to avoid duplicated data in Redux, In more explanation Redux state can be like a database and selectors like SELECT queries to get selected filter data from database.
Like what we have in this code challenge, GroupBy, sortBy or filterBy values would be stored in Redux and a selector would be used to compute the filtered reviews list from the Redux state.
Also I was trying to avoid transforming data in the components this makes more coupled to redux state and less generic/reusable.

> “As [Dan Abramov](https://twitter.com/dan_abramov) points out, it makes sense to keep selectors near reducers because they operate on the same state. If the state schema changes, it is easier to update the selectors than to update the components.”


##### Components
Components are the key element of the application, and their importance comes from reusability. Components must be decoupled from the data itself, and should be generic to handle many types of data according to their instructions.

##### Containers
Containers are opposite when it comes to handling data, Containers must be aware of the type of data and act upon them. That's why I use stateful components to represent containers because they care about the application state. On the other hand, components are stateless

---

##### Installation

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`


---


##### Main Dependencies :eyes: :eyes:

| **Dependency** | **Use** |
|----------|-------|
|@material-ui| UI framework|
|react|React Library|
|react-dom|React library for DOM rendering|
|redux|Library for unidirectional data flows|
|react-redux|Redux library for connecting React components to Redux|
|redux-promise|Promise middleware for Redux|
|redux-thunk|Async Redux wrapping action creators|
|reselect|library for Redux|
|prop-types|Runtime type checking for React props|
|dayjs| JavaScript library that parse date, Fast 2kB alternative to Moment.js with the same modern API|
|lodash.groupby|JavaScript utility library for grouping reviews list|
|react-infinite-scroller|Infinite scroll Component|