import React, { Fragment } from "react";
import { Provider } from "react-redux";
import createStore from "./reducers";
import { VisibleReivewsList, FiltersPanelContainer } from "./containers";

const store = createStore();

const App = () => (
  <Provider store={store}>
    <Fragment>
      <FiltersPanelContainer />
      <VisibleReivewsList />
    </Fragment>
  </Provider>
);

export default App;
