import * as types from "./types";

export const fetchReviewsData = page => async dispatch => {
  try {
    dispatch({ type: types.FETCH_REVIEWS_REQUEST });
    const response = await fetch(`/reviews/${page}`);
    const payload = await response.json();
    dispatch({type: types.FETCH_REVIEWS_SUCCESS, payload });
  } catch (payload) {
    dispatch({type: types.FETCH_REVIEWS_FAILURE, payload });
  }
};