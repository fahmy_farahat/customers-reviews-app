import fetchMock from "fetch-mock";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { fetchReviewsData } from "./reviews-actions";
import * as types from "./types";

const mockStore = configureMockStore([thunk]);

describe("Reviews actions", () => {
  afterEach(() => fetchMock.restore());

  test("should handle fetch reviews success", () => {
    const mockReviews = {
      reviews: [{
        reviewId: "id4444",
        childAsin: "333333",
        title: "title",
        content: "text",
        "stars": 5,
        reviewCreated: 1512432000000,
        productTitle: "Ptitle",
      }],
      hasMore: false
    };

    fetchMock.get(`/reviews/1`, {
      body: mockReviews,
    });
    
    const store = mockStore({});
    const expectedActions = [
      { type: types.FETCH_REVIEWS_REQUEST },
      { type: types.FETCH_REVIEWS_SUCCESS,
        payload: mockReviews 
      }
    ];

    store.dispatch(fetchReviewsData(1))
    .then(() => {
      const actions = store.getActions();
      expect(actions).toEqual(expectedActions);
    });
  });

  test("should handle fetch reviews failure", () => {
    fetchMock.get(`/reviews/1`, { throws: "ERROR" });
    const store = mockStore({});
    const expectedActions = [
      { type: types.FETCH_REVIEWS_REQUEST },
      { type: types.FETCH_REVIEWS_FAILURE,
        payload: "ERROR" 
      }
    ];
    
    store.dispatch(fetchReviewsData(1))
    .then(() => {
      const actions = store.getActions();
      expect(actions).toEqual(expectedActions);
    });
  });
});