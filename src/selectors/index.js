import { createSelector } from 'reselect'
import { DATE_FORMATS } from "../constants";
import dayjs from "dayjs";
import _groupby from "lodash.groupby";

const getStateFilters = state => state.filters;
const getStateReviews = state => state.reviews.data;

const getDateFormat = (date, groupBy, dateFormat) => {
  const startDate = date.startOf(groupBy).format(dateFormat)

  return groupBy === 'week'
    ? `${startDate} - ${date.endOf(groupBy).format(dateFormat)}`
    : startDate;
}

const getFilteredGroups = (reviews, groupby) => {
  return groupby
  ? _groupby(reviews, review => {
      const dateFormat = DATE_FORMATS[groupby];
      const date = dayjs(review.reviewCreated);

      return getDateFormat(date, groupby, dateFormat);
    })
  : {};
}

export const getVisibleReviews = createSelector(
  [ getStateReviews, getStateFilters ],
  (reviews, filters) => {
    const { sortby, rating, groupby } = filters;
    
    const filterByRating = review => rating[review.stars];
    
    const sortedList = reviews.sort((a, b) => (
      sortby === 'desc' 
        ? a.reviewCreated - b.reviewCreated
        : b.reviewCreated - a.reviewCreated
    ));
    
    const filteredList = sortedList.filter(filterByRating)
  
    return {
      filteredList, 
      filteredGroup: getFilteredGroups(filteredList, groupby)
    };
  }
);