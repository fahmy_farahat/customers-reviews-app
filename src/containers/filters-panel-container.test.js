import React from "react";
import { mount } from "enzyme";
import FiltersPanelContainer from "./filters-panel-container";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { FiltersPanel } from "../components";

describe("FiltersPanelContainer - General", () => {
  let wrapper, store;
  const mockFilters = {
    groupby: "",
    sortby: "asc",
    search: "",
    rating: {
      1: false,
      2: false,
      3: false,
      4: false,
      5: true
    }
  };
  beforeEach(()=> {
    const mockStore = configureMockStore([thunk]);
    const initialState = { 
      filters: mockFilters
    };
    store = mockStore(initialState);
    wrapper = mount(<FiltersPanelContainer store={store}/>);
  });
  
  afterEach(() => {
    wrapper.unmount();
  });

  test("render without crashing", () => {
    expect(wrapper).toHaveLength(1);
  });
  
  test("should render FiltesPanelComponent props", () => {
    expect(wrapper.find(FiltersPanel).props().filters).toEqual(mockFilters);
  });
  
});
