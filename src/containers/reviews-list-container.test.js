import React from "react";
import { mount } from "enzyme";
import ReviewsListContainer from "./reviews-list-container";
import { ReviewsListView, ReviewBoxView } from "../components";

describe("ReviewsListContainer - General", () => {
  let mochReviews, mockFunc;
  beforeEach(()=> {
    mockFunc = jest.fn();
    mochReviews = {
      filteredGroup: {},
      filteredList: [],
    };
  });

  it("Should render without crashing", () => {
    const props = {
      reviews: mochReviews,
      fetchReviews: mockFunc,
      hasMore: false,
      isLoading: false,
      error: null
    };
    const wrapper = mount(<ReviewsListContainer  {...props}/>);
    expect(wrapper).toHaveLength(1);
    wrapper.unmount();
  });
  
  it("Should render Reviews list", () => {
    const mockReviewData = {
      reviewId: "id4444",
      childAsin: "333333",
      title: "title",
      content: "text",
      "stars": 5,
      reviewCreated: 1512432000000,
      productTitle: "Ptitle",
    };
    const props = {
      reviews: {
        ...mochReviews,
        filteredList: [mockReviewData],   
      },
      fetchReviews: mockFunc,
      hasMore: false,
      isLoading: false,
      error: null
    };
    const wrapper = mount(<ReviewsListContainer  {...props}/>);
    expect(wrapper.find(ReviewsListView)).toHaveLength(1);
    expect(wrapper.find(ReviewsListView).props().reviews).toHaveLength(1);
    
    const reviewComponent = wrapper.find(ReviewBoxView);
    expect(reviewComponent.props().review).toEqual(mockReviewData);
    wrapper.unmount();
  });
});
