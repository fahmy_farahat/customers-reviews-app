import { connect } from "react-redux";
import { FiltersPanel } from "../components";
import {
  SET_GROUPING_FILTER,
  SET_SORTING_FILTER,
  SET_RATING_FILTER,
  RESET_FILTER
 } from "../actions/types";

 const mapStateToProps = ({ filters }) => ({
  filters,
});

const mapDispatchToProps = dispatch => ({
  onGroupingChange: filter => dispatch({ type: SET_GROUPING_FILTER, filter }),
  onSortingChange: filter => dispatch({ type: SET_SORTING_FILTER, filter }),
  onRatingChange: filter => dispatch({ type: SET_RATING_FILTER, filter }),
  onRefresh: () => dispatch({ type: RESET_FILTER })
});

const FiltersPanelContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FiltersPanel);

export default FiltersPanelContainer;