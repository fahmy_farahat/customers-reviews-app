import { connect } from "react-redux";
import { fetchReviewsData } from "../actions";
import ReviewsListContainer from "./reviews-list-container";
import { getVisibleReviews } from "../selectors";

const mapStateToProps = state => ({
  reviews: getVisibleReviews(state),
  isLoading: state.reviews.isLoading,
  error: state.reviews.error, 
  hasMore: state.reviews.hasMore,
  pagePaginat: state.pagePaginat
});

const mapDispatchToProps = dispatch => ({
  fetchReviews: page => dispatch(fetchReviewsData(page))
});

const VisibleReivewsList = connect(
  mapStateToProps, 
  mapDispatchToProps
)(ReviewsListContainer);

export default VisibleReivewsList;