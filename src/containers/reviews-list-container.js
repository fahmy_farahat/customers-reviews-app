import React from "react";
import PropTypes from "prop-types";
import InfiniteScroll from 'react-infinite-scroller';
import { ReviewsListView } from "../components"
import Container from "@material-ui/core/Container";

class ReviewsListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
    }
  };
  
  componentDidMount () {
    const { page } = this.state;
    this.props.fetchReviews(page);
  };

  renderReviewsGroupList = filteredGroup => (
    Object.keys(filteredGroup).map(date => (
      <ReviewsListView key={date} date={date} reviews={filteredGroup[date]}/>        
  )));

  render() {
    const { error, hasMore, isLoading, fetchReviews } = this.props;
    const { filteredGroup, filteredList } = this.props.reviews;
    const { page } = this.state;
    return (
      <InfiniteScroll
        pageStart={page}
        initialLoad={false}
        loadMore={nextPage => fetchReviews(nextPage)}
        hasMore={hasMore}
      >
        <Container maxWidth="md">
          {
            Object.keys(filteredGroup).length
              ? (this.renderReviewsGroupList(filteredGroup))
              : (<ReviewsListView reviews={filteredList}/>) 
          }
          {!hasMore && !isLoading && !error && (<center>No more reviews</center>)}
          {isLoading && (<center>Loading.....</center>)}
          {error && (<center>Error</center>)}
        </Container>
      </InfiniteScroll>
    )
  }
};

ReviewsListContainer.propTypes = {
  fetchReviews: PropTypes.func.isRequired,
  reviews: PropTypes.shape({
    filteredGroup: PropTypes.object.isRequired,
    filteredList: PropTypes.array.isRequired
  }).isRequired,
  hasMore: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.object
};

export default ReviewsListContainer;
