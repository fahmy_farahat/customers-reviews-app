export const DATE_FORMATS = {
  day: 'DD.MM',
  week: 'DD.MM',
  month: 'MMMM',
};
