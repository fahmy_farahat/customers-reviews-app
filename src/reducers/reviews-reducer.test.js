import ReviewsReducer from "./reviews-reducer";
import {
  FETCH_REVIEWS_REQUEST, 
  FETCH_REVIEWS_SUCCESS
} from "../actions/types";

describe("ReviewsReducer - General", () => {
  test("should return the initial state", () => {
    const expactedInitialState = { 
      data: [], 
      isLoading: false,
      hasMore: false, 
      error: null
    };
    expect(ReviewsReducer(undefined, {})).toEqual(expactedInitialState);    
  });

  test("should handle fetch Reviews request", () => {
    const startAction = {
      type: FETCH_REVIEWS_REQUEST
    };
    const expacted = { isLoading: true, hasMore: false };
    expect(ReviewsReducer({}, startAction)).toEqual(expacted);
  });

  test("should handle fetch reviews success", () => {
    const mockReviews = {
      reviews: [{
        reviewId: "id4444",
        childAsin: "333333",
        title: "title",
        content: "text",
        "stars": 5,
        reviewCreated: 1512432000000,
        productTitle: "Ptitle",
      }],
      hasMore: false
    };
    const initialState = { 
      data: []
    };
    const successAction = {
      type: FETCH_REVIEWS_SUCCESS,
      payload: mockReviews
    };
    const expacted = {
      data: mockReviews.reviews,
      hasMore: mockReviews.hasMore, 
      isLoading: false,
    };
    expect(ReviewsReducer(initialState, successAction)).toEqual(expacted);
  });
});