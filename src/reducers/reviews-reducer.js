import {
  FETCH_REVIEWS_REQUEST, 
  FETCH_REVIEWS_SUCCESS, 
  FETCH_REVIEWS_FAILURE
} from "../actions/types";

const initialState = { 
  data: [], 
  isLoading: false,
  hasMore: false, 
  error: null
};

export default function ReviewsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_REVIEWS_REQUEST:
      return {
        ...state,
        isLoading: true,
        hasMore: false
      };
    case FETCH_REVIEWS_SUCCESS:
      return {
        ...state,
        data: [
          ...state.data,
          ...payload.reviews
        ],
        hasMore: payload.hasMore,
        isLoading: false
      };
    case FETCH_REVIEWS_FAILURE:
      return {
        ...state,
        error: payload,
        isLoading: false
      };
    default:
      return state;
  }
}