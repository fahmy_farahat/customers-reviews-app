import { combineReducers } from "redux";
import { createStore, applyMiddleware, compose } from "redux";
import Promise from "redux-promise";
import thunk from "redux-thunk";
import ReviewsReducer from "./reviews-reducer";
import FiltersReducer from "./filters-reducer";

const rootReducer = combineReducers({
  reviews: ReviewsReducer,
  filters: FiltersReducer
});

export default (reducer = rootReducer) => {
  return createStore(reducer, compose(applyMiddleware(...[thunk, Promise])));
};