import FiltersReducer from "./filters-reducer";
import {
  SET_GROUPING_FILTER,
  SET_SORTING_FILTER,
} from "../actions/types";

describe("FiltersReducer - General", () => {
  test("should return the initial state", () => {
    const expactedInitialState = { 
      groupby: "",
      sortby: "asc",
      rating: {
        1: false,
        2: false,
        3: false,
        4: false,
        5: true
      }
    };
    expect(FiltersReducer(undefined, {})).toEqual(expactedInitialState);    
  });

  test("should set gouping filter", () => {
    const startAction = {
      type: SET_GROUPING_FILTER,
      filter: "day"
    };
    const expacted = { groupby: "day", };
    expect(FiltersReducer({}, startAction)).toEqual(expacted);
  });

  test("should set sortby asc filter", () => {
    const startAction = {
      type: SET_SORTING_FILTER,
      filter: "asc"
    };
    const expacted = { sortby: "asc", };
    expect(FiltersReducer({}, startAction)).toEqual(expacted);
  });
});