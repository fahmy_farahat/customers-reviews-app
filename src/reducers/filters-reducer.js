import {
  SET_GROUPING_FILTER,
  SET_SORTING_FILTER,
  SET_RATING_FILTER,
  RESET_FILTER
} from "../actions/types";

const initialState = { 
  groupby: "",
  sortby: "asc",
  rating: {
    1: false,
    2: false,
    3: false,
    4: false,
    5: true
  }
}
export default function FiltersReducer(state = initialState , { type, filter }) {
      switch (type) {
    case SET_GROUPING_FILTER:
      return {
        ...state,
        groupby: filter
      };
    case SET_SORTING_FILTER:
        return {
          ...state,
          sortby: filter
        };
    case SET_RATING_FILTER:
      return {
        ...state,
        rating: {
          ...state.rating,
          ...filter
        }
      };
    case RESET_FILTER:
      return initialState;
    default:
      return state;
  }
}