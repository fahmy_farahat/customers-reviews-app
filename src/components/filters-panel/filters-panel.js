import React from 'react';
import PropTypes from "prop-types";
import { Box, Grid, Button } from '@material-ui/core';
import CheckBoxsView from "../check-boxs-view/check-boxs-view";
import SelectInputBox from "../select-input-box/select-input-box";
import Container from '@material-ui/core/Container';

const FiltersPanel = ({ filters, onGroupingChange, onSortingChange, onRatingChange, onRefresh }) => (
  <Container maxWidth="md">
    <Box p={2} mb={2}>
      <Grid container spacing={2}>
        <Grid item xs={6} sm={3}>
          <SelectInputBox 
            value={filters.groupby}
            onChange={e => onGroupingChange(e.target.value)}
            inputLabel="Group by"
            inputName="groupby"
            selectOptions={[{
              label: "none",
              value:""
            },{
              label: "Day",
              value: "day"
            },{
              label: "Week",
              value: "week"
            },{
              label: "Month",
              value: "month"
            }
            ]}
          />
        </Grid>
        <Grid item xs={6} sm={3}>
          <SelectInputBox 
            value={filters.sortby}
            onChange={e => onSortingChange(e.target.value)}
            inputLabel="Order by"
            name="sortby"
            selectOptions={[
              {
                label: "Most recent",
                value:"asc"
              },
              {
                label: "Last",
                value: "desc"
              }
            ]}
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <CheckBoxsView 
            filter={filters.rating} 
            onChange={e => onRatingChange({[e.target.name]: e.target.checked})}
          />
        </Grid>
        <Grid item xs={4} sm={4}>
          <Button onClick={() => onRefresh()} variant="contained" size="small" color="primary">
            Refresh
          </Button>
        </Grid>
      </Grid>
    </Box>
  </Container>
);

FiltersPanel.propTypes = {
  filters: PropTypes.shape({
    sortby: PropTypes.oneOf(["asc", "desc"]).isRequired,
    groupby: PropTypes.string.isRequired,
    rating: PropTypes.object.isRequired,
  }).isRequired,
  onGroupingChange: PropTypes.func.isRequired, 
  onSortingChange: PropTypes.func.isRequired,
  onRatingChange: PropTypes.func.isRequired,
  onRefresh: PropTypes.func.isRequired
};

export default FiltersPanel;