import React from "react";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
import FiltersPanel from "./filters-panel";

describe("FiltersPanel component", () => {
  test("without crashing", () => {
    const mockFun = jest.fn();
    const props = {
      filters: {
        sortby: "asc",
        groupby: "",
        rating: {}
      },
      onGroupingChange: mockFun, 
      onSortingChange: mockFun, 
      onRatingChange: mockFun, 
      onRefresh: mockFun
    }
    const wrapper = shallow(<FiltersPanel {...props} />);
    expect(wrapper).toHaveLength(1);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
