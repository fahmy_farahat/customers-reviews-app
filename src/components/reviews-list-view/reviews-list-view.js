import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { ReviewBoxView } from "../";
import { Box, Typography } from "@material-ui/core/";

const ReviewsListView = ({ reviews, date }) => (
  <Fragment>
      <Box pt={1} pl={1}>
        <Typography variant="button" display="block" gutterBottom>
          {date}
        </Typography>
      </Box>
      {reviews.map((review, i) => (
        <ReviewBoxView key={i} review={review} />
      ))}
  </Fragment>
);

ReviewsListView.propTypes = {
  reviews: PropTypes.array.isRequired,
  date: PropTypes.string
};

export default ReviewsListView;