import React from "react";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
import ReviewsListView from "./reviews-list-view";

describe("ReviewsListView component", () => {
  test("render ReviewsListView without crashing", () => {
    const props = {
      reviews: [{
        reviewCreated: 1568419200,
        stars: 5,
        childAsin: "test",
        productTitle: "titleTest",
        content: "contentTest",
      }]
    };
    const wrapper = shallow(<ReviewsListView {...props} />);
    expect(wrapper).toHaveLength(1);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
