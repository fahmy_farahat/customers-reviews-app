import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarIcon from '@material-ui/icons/Star';

const StarCheckBox = props => (
  <Checkbox {...props} icon={<StarBorderIcon />} checkedIcon={<StarIcon color="primary" />} />
);

export default StarCheckBox;