import React from 'react';
import PropTypes from "prop-types";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem
} from '@material-ui/core';

const SelectInputBox = ({ inputLabel, inputName, onChange, value, selectOptions }) => (
  <FormControl fullWidth variant="filled">
    <InputLabel>{inputLabel}</InputLabel>
    <Select
      value={value}
      onChange={onChange}
      inputProps={{"name": inputName}}
    >
      {selectOptions.map((item, index) => (
        <MenuItem key={index} value={item.value}>{item.label}</MenuItem>
      ))}
    </Select>
  </FormControl>
);

SelectInputBox.propTypes = {
  inputName: PropTypes.string,
  inputLabel: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  selectOptions: PropTypes.array.isRequired
};

export default SelectInputBox;