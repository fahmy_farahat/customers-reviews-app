import ReviewBoxView from "./review-box-view/review-box-view";
import ReviewsListView from "./reviews-list-view/reviews-list-view";
import FiltersPanel from "./filters-panel/filters-panel";

export { 
  ReviewBoxView,
  ReviewsListView,
  FiltersPanel
};