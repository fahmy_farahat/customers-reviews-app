import React from 'react';
import PropTypes from "prop-types";
import { Box, Grid, Typography } from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import dayjs from "dayjs";

//TODO:: move to utilities..
const dateFormat = ts => dayjs(ts).format('DD.MM.YYYY');

const ReviewBoxView = ({ review }) => (
    <Box boxShadow={5} p={3} pl={4} mt={1} mb={3} borderRadius={2}>
      <Grid container spacing={2}>
        <Grid item xs={3} sm={2}>
          <img alt="user" src="https://picsum.photos/id/237/64/64" />
        </Grid>
        <Grid item xs={4} sm={3}>
          <Typography variant="subtitle1">
            Date
          </Typography>
          <Typography variant="body2">
            {dateFormat(review.reviewCreated)}
          </Typography>
        </Grid>
        <Grid item xs={4} sm={3}>
          <Typography variant="subtitle1">
            Stars
          </Typography>
          <Rating
            name="stars"
            readOnly
            value={review.stars}
            emptyIcon={<StarBorderIcon />}
          />
        </Grid>
        <Grid item xs={6} sm={3} >
          <Typography gutterBottom variant="subtitle2">
            {review.childAsin}
          </Typography>
          <Typography noWrap variant="body2">
            {review.productTitle}
          </Typography>
        </Grid>
      </Grid>
      <Grid>
        <Box mt={3}>
          <Typography gutterBottom variant="subtitle2">
            {review.title}
          </Typography>
          <Typography variant="caption">
            {review.content}
          </Typography>
        </Box>
      </Grid>

    </Box>
);

ReviewBoxView.propTypes = {
  review: PropTypes.shape({
    reviewCreated: PropTypes.number.isRequired,
    stars: PropTypes.number.isRequired,
    childAsin: PropTypes.string.isRequired,
    productTitle: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
  }).isRequired
};

export default ReviewBoxView;