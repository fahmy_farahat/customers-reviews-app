import React from "react";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
import ReviewBoxView from "./review-box-view";

describe("ReviewBoxView component", () => {
  test("without crashing", () => {
    const props = {
      review: {
        reviewCreated: 1568419200,
        stars: 5,
        childAsin: "test",
        productTitle: "titleTest",
        content: "contentTest",
      }
    };
    const wrapper = shallow(<ReviewBoxView {...props} />);
    expect(wrapper).toHaveLength(1);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
