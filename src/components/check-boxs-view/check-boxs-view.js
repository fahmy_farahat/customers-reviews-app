import React from "react";
import { Box, FormGroup, FormControlLabel, FormLabel } from '@material-ui/core';
import StarCheckBox from "../star-check-box/star-check-box";

const CheckBoxsView = ({ onChange, filter }) => {
  return (
    <FormGroup>
      <FormLabel component="legend">Filter by</FormLabel>
      <Box>
        {Object.keys(filter).map(rating => (
          <FormControlLabel
            key={rating}
            control={
              <StarCheckBox 
                checked={!!filter[rating]} 
                value={rating} 
                name={rating} 
                onChange={onChange} 
              />
            }
            label={rating}
          />
        ))}
      </Box>

    </FormGroup>
  )
};

export default CheckBoxsView;